#!/bin/bash
export NODE_ENV="production"
cd /srv/www/nashipunch-ograph/
git fetch origin master
git reset --hard HEAD
git pull
yarn install
sudo /bin/systemctl restart knockout-ograph