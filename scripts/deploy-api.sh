#!/bin/bash
set -e
export NODE_ENV="production"
cd /srv/www/nashipunch-api/
git fetch origin master
git reset --hard HEAD
git checkout "${1:-"origin/master"}"
# Install in dev mode to include the TypeScript compiler
yarn install --production=false
# run migrations
yarn sequelize db:migrate
yarn build
sudo /bin/systemctl restart knockout-api