#!/bin/bash
set -e
export NODE_ENV="production"
cd /srv/www/nashipunch-front/
git fetch origin master
git reset --hard HEAD
git checkout "${1:-"origin/master"}"
yarn install --production=false
yarn run build:prod
sudo /bin/systemctl restart knockout-front