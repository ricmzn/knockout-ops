# Knockout Ops

Miscellaneous files related to deployment and operations

## System files

* logrotate files belong within /etc/logrotate.d
* nginx files belong within /etc/nginx/sites-available and they should be symlinked individually to /etc/nginx/sites-enabled
* systemd unit files belong within /etc/systemd/system
* sudoers are to be appended to /etc/sudoers or placed within /etc/sudoers.d

## Automated deployment setup

* download and install: https://github.com/adnanh/webhook
* place the hooks.json file and the deploy-*.sh scripts in the root of /home/knockout
* replace all of the "TOKEN PLACEHOLDER" entries in hooks.json with a single securely-generated random value
  - to generate: `node -p "require('crypto').randomBytes(64).toString('base64')"`
* (optional) generate self-signed certs for the webhook service
  - to generate: `openssl req -new -newkey rsa:4096 -x509 -sha256 -days 36500 -nodes -out cert.pem -keyout key.pem -subj "/CN=hostname"`
* ensure the knockout-api and knockout-front projects are set up to call the webhooks on deployment (see .gitlab-ci.yml in each project)
* enable the knockout-hooks service
